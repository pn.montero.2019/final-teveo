from django.db import models


# Create your models here.


class Camera(models.Model):
   identifier = models.CharField(max_length=100, unique=True)  # identificador de la cámara
   source = models.CharField(max_length=100)   # fuente de datos de donde se obtiene la imagen
   location_name = models.CharField(max_length=100)
   latitude = models.FloatField()
   longitude = models.FloatField()
   image_url = models.URLField()   # URL de la imagen
   votos = models.IntegerField(default=0)


   def __str__(self):
       return f"Identifier: {self.identifier}, Source: {self.source}, Location Name: {self.location_name}, Latitude: {self.latitude}, Longitude: {self.longitude}, Image URL: {self.image_url}, Votos: {self.votos}"


class Comentario(models.Model):
   camara = models.ForeignKey(Camera, on_delete=models.CASCADE, related_name='comentarios')
   fecha_creacion = models.DateTimeField(auto_now_add=True)
   texto = models.TextField()
   imagen_camara = models.ImageField(upload_to='', null=True, blank=True)


   # Define el orden predeterminado de los registros para consultas de la base de datos.
   # Los registros se ordenarán por fecha de creación en orden descendente (más reciente primero).
   class Meta:
       ordering = ['-fecha_creacion']


   # nos permitirá obtener los comentarios de una cámara específica ordenados por fecha de creación descendente
   @classmethod
   def obtener_comentarios_ordenados(cls, camara):
       return cls.objects.filter(camara=camara).order_by('-fecha_creacion')




class Configuracion(models.Model):
   nombre_comentador = models.CharField(max_length=100)
   tamanio_fuente = models.CharField(max_length=100)
   tipo_fuente = models.CharField(max_length=100)


class SessionData(models.Model):
   id = models.UUIDField(primary_key=True, editable=False)
   data = models.JSONField()



