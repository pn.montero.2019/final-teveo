from django.urls import path
from . import views


urlpatterns = [
   path('', views.pagina_principal, name='pagina_principal'),
   path('camaras/', views.pagina_camaras, name='pagina_camaras'),
   path('camaras/<str:identifier>/', views.pagina_camara, name='pagina_camara'),
   path('camaras/dinamica/<int:camara_id>/', views.pagina_dinamica_camara, name='pagina_dinamica_camara'),
   path('camaras/formulario_comentario/<int:camara_id>/', views.formulario_comentario,
        name='formulario_comentario'),
   path('camaras/comentarios/<int:camara_id>/', views.obtener_comentarios, name='obtener_comentarios'),
   path('comentario/', views.poner_comentario, name='poner_comentario'),
   path('camaras/<str:identifier>/votar/', views.votar_camara, name='votar_camara'),
   path('configuracion/', views.pagina_configuracion, name='pagina_configuracion'),
   path('ayuda/', views.pagina_ayuda, name='pagina_ayuda'),
   path('descargar_datos/', views.descargar_datos, name='descargar_datos'),
   path('logout/', views.cerrar_sesion, name='cerrar_sesion'),
   path('camaras/<str:identifier>/json', views.camara_json, name='camara_json'),
   path('generar_enlace_autenticacion/', views.generar_enlace_autenticacion, name='generar_enlace_autenticacion'),
]



