from django.test import TestCase
from django.urls import reverse
from .models import Comentario, Camera, Configuracion, SessionData

"""Creamos un objeto Camera para las pruebas"""
def setup_camara():
    # Crear un objeto Camera para la prueba
    camara = Camera.objects.create(
        identifier='LIS2-Atest',
        source='Fuente de prueba',
        location_name='CAMARA-CGT VALLADOLID test',
        latitude=41.9811,
        longitude=-4.4348,
        image_url='http://infocar.dgt.es/etraffic/data/camaras/3.jpg',
        votos=5
    )
    return camara


"""Test para la página principal"""
class PaginaPrincipalTest(TestCase):
    def setUp(self):
        # Crear un objeto Camera
        self.camara = setup_camara()

        # Crear varios objetos Comentario asociados con el objeto Camera
        for _ in range(6):  # Crear más de 5 comentarios para probar la paginación
            Comentario.objects.create(texto='Comentario de prueba', fecha_creacion='2022-01-01', camara=self.camara)

    def test_pagina_principal_carga_correctamente(self):
        # Simular una solicitud GET a la página principal
        response = self.client.get(reverse('pagina_principal'))

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Comprobar que la respuesta contiene ciertos textos
        self.assertContains(response, 'Página Principal')
        self.assertContains(response, 'Comentarios en General')

        # Comprobar que la respuesta contiene el texto de los comentarios
        for comentario in Comentario.objects.all():
            self.assertContains(response, comentario.texto)


"""Test para la página de cámaras"""
class PaginaCamarasTest(TestCase):
    def setUp(self):
        # Crear varios objetos Camera
        for i in range(5):  # Crear más de 4 cámaras para probar la paginación
            Camera.objects.create(
                identifier=f'LIS2-Atest{i}',
                source='Fuente de prueba',
                location_name=f'CAMARA-CGT VALLADOLID_{i} test',
                latitude=41.9811,
                longitude=-4.4348,
                image_url='http://infocar.dgt.es/etraffic/data/camaras/3.jpg',
                votos=5
            )

    def test_pagina_camaras_carga_correctamente(self):
        # Simular una solicitud GET a la página de cámaras
        response = self.client.get(reverse('pagina_camaras'))

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Comprobar que la respuesta contiene ciertos textos
        self.assertContains(response, 'Página de Cámaras')
        self.assertContains(response, 'Fuentes de Datos')

        # Comprobar que la respuesta contiene el identificador de cada cámara en la primera página
        for camara in Camera.objects.all()[:4]:  # Solo las primeras 4 cámaras deberían estar en la primera página
            self.assertContains(response, camara.identifier)


"""Test para la página dinámica de cada cámara"""
class PaginaDinamicaCamaraTest(TestCase):
    def setUp(self):
        self.camara = setup_camara()

    def test_pagina_dinamica_camara_carga_correctamente(self):
        # Simular una solicitud GET a la página dinámica de la cámara
        response = self.client.get(reverse('pagina_dinamica_camara', args=[self.camara.id]))

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Comprobar que la respuesta contiene ciertos textos
        self.assertContains(response, f'{self.camara.identifier} - Página Dinámica de Cámara')
        self.assertContains(response, self.camara.identifier)


"""Test para la página de cada cámara"""
class PaginaCamaraTest(TestCase):
    def setUp(self):
        # Crear un objeto Camera para la prueba
        self.camara = setup_camara()

    def test_pagina_camara_carga_correctamente(self):
        # Simular una solicitud GET a la página de la cámara
        response = self.client.get(reverse('pagina_camara', args=[self.camara.identifier]))

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Comprobar que la respuesta contiene ciertos textos
        self.assertContains(response, 'Detalles de Cámara')
        self.assertContains(response, self.camara.identifier)


"""Test para el formulario de comentario de una cámara"""
class FormularioComentarioTest(TestCase):
    def setUp(self):
        # Crear un objeto Camera para la prueba
        self.camara = setup_camara()

    def test_formulario_comentario_carga_correctamente(self):
        # Simular una solicitud GET al formulario de comentario
        response = self.client.get(reverse('formulario_comentario', args=[self.camara.id]))

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Comprobar que la respuesta contiene ciertos textos
        self.assertContains(response, 'Poner Comentario')
        self.assertContains(response, self.camara.identifier)


"""Test para la página de comentarios de una cámara"""
class ObtenerComentariosTest(TestCase):
    def setUp(self):
        # Crear un objeto Camera para la prueba
        self.camara = setup_camara()

    def test_obtener_comentarios_carga_correctamente(self):
        # Simular una solicitud GET a obtener_comentarios
        response = self.client.get(reverse('obtener_comentarios', args=[self.camara.id]))

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)


"""Test para la página de poner un comentario en una cámara"""

class PonerComentarioTest(TestCase):
    def setUp(self):
        self.camara = setup_camara()

    def test_poner_comentario(self):
        response = self.client.post(
            reverse('poner_comentario') + f'?identifier={self.camara.identifier}',
            data={
                'texto': 'Este es un comentario de prueba',
            }
        )

        self.assertEqual(response.status_code, 302)  # La vista redirige después de un POST exitoso, por lo que el código de estado esperado es 302

        self.assertTrue(
            Comentario.objects.filter(
                camara_id=self.camara.id,
                texto='Este es un comentario de prueba',
            ).exists()
        )


"""Test para el recurso votar cámara"""
class VotarCamaraTest(TestCase):
    def setUp(self):
        # Crear un objeto Camera para la prueba
        self.camara = setup_camara()

    def test_votar_camara(self):
        # Simular una solicitud POST a votar_camara
        response = self.client.post(
            reverse('votar_camara', args=[self.camara.identifier])
        )

        # Comprobar que la respuesta tiene un código de estado 302 (Redirección)
        self.assertEqual(response.status_code, 302)

        # Recargar el objeto camara de la base de datos
        self.camara.refresh_from_db()

        # Comprobar que los votos de la cámara se han incrementado correctamente
        self.assertEqual(self.camara.votos, 6)


"""Test para la página de configuración"""
class PaginaConfiguracionTest(TestCase):
    def test_pagina_configuracion_get(self):
        # Simular una solicitud GET a pagina_configuracion
        response = self.client.get(reverse('pagina_configuracion'))

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)

    def test_pagina_configuracion_post(self):
        # Simular una solicitud POST a pagina_configuracion
        response = self.client.post(
            reverse('pagina_configuracion'),
            data={
                'nombre_comentador': 'Comentador de prueba',
                'tamanio_fuente': 'grande',
                'tipo_fuente': 'arial',
            }
        )

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Comprobar que la configuración se ha guardado correctamente en la sesión
        self.assertEqual(self.client.session['nombre_comentador'], 'Comentador de prueba')
        self.assertEqual(self.client.session['tamanio_fuente'], 20)
        self.assertEqual(self.client.session['tipo_fuente'], 'arial')


"""Test para la descarga de datos"""
class DescargarDatosTest(TestCase):
    def test_descargar_datos_post(self):
        # Simular una solicitud POST a descargar_datos
        response = self.client.post(
            reverse('descargar_datos'),
            data={'fuente_datos': 'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml'}
        )

        # Comprobar que la respuesta redirige a la página de cámaras
        self.assertRedirects(response, reverse('pagina_camaras'))

        # Comprobar que se crean nuevas cámaras en la base de datos
        self.assertTrue(Camera.objects.filter(identifier='LIS1-1').exists())


"""Test para el recurso de cerrar sesión"""
class CerrarSesionTest(TestCase):
    def test_cerrar_sesion_get(self):
        # Primero, debes iniciar sesión con un usuario de prueba
        self.client.login(username='teveo', password='teveo')

        # Simular una solicitud GET a cerrar_sesion
        response = self.client.get(reverse('cerrar_sesion'))

        # Comprobar que la respuesta redirige a la página principal
        self.assertRedirects(response, reverse('pagina_principal'))

        # Comprobar que el usuario ya no está autenticado
        self.assertFalse(response.wsgi_request.user.is_authenticated)


"""Test para el recurso de cámara en formato JSON"""
class CamaraJsonTest(TestCase):
    def setUp(self):
        # Crear un objeto Camera para la prueba
        self.camara = setup_camara()

    def test_camara_json_get(self):
        # Simular una solicitud GET a camara_json
        response = self.client.get(reverse('camara_json', args=[self.camara.identifier]))

        # Comprobar que la respuesta tiene un código de estado 200 (OK)
        self.assertEqual(response.status_code, 200)

        # Comprobar que la respuesta tiene el tipo de contenido correcto
        self.assertEqual(response['Content-Type'], 'application/json')

        # Comprobar que los datos devueltos son correctos
        data = response.json()
        self.assertEqual(data['nombre'], self.camara.identifier)
        self.assertEqual(data['descripcion'], self.camara.location_name)
        self.assertEqual(data['numero_comentarios'], self.camara.comentarios.count())
        self.assertEqual(data['latitude'], self.camara.latitude)
        self.assertEqual(data['longitude'], self.camara.longitude)
        self.assertEqual(data['image_url'], self.camara.image_url)
        self.assertEqual(data['votos'], self.camara.votos)


"""Test para el recurso de generar enlace de autenticación"""
class GenerarEnlaceAutenticacionTest(TestCase):
    def setUp(self):
        # Crear un objeto Configuracion para la prueba
        self.configuracion = Configuracion.objects.create(
            nombre_comentador='usuario_prueba',
            tipo_fuente='Arial',
            tamanio_fuente='12'
        )

    def test_generar_enlace_autenticacion_get(self):
        # Iniciar sesión con un usuario de prueba y establecer algunos datos de sesión
        self.client.login(username='usuario_prueba', password='contraseña_prueba')
        session = self.client.session
        session['configuracion_id'] = self.configuracion.id
        session['nombre_comentador'] = 'usuario_prueba'
        session['tipo_fuente'] = 'Arial'
        session['tamanio_fuente'] = '12'
        session.save()

        # Simular una solicitud GET a generar_enlace_autenticacion
        response = self.client.get(reverse('generar_enlace_autenticacion'))

        # Comprobar que la respuesta redirige a la página principal con el identificador de autorización en la cadena de consulta
        self.assertEqual(response.status_code, 302)
        self.assertTrue('id=' in response.url)

        # Comprobar que se crea un nuevo objeto SessionData en la base de datos con los datos correctos
        id_autorizacion = response.url.split('=')[1]
        session_data = SessionData.objects.get(id=id_autorizacion)
        self.assertEqual(session_data.data['configuracion_id'], self.configuracion.id)
        self.assertEqual(session_data.data['nombre_comentador'], 'usuario_prueba')
        self.assertEqual(session_data.data['tipo_fuente'], 'Arial')
        self.assertEqual(session_data.data['tamanio_fuente'], '12')
