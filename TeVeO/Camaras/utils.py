import requests
from xml.etree import ElementTree


from .models import Camera, Comentario


"""Funciones extras para la descarga y procesamiento de datos"""
def descargar_fuente(url):
   response = requests.get(url)
   if response.status_code == 200:
       #print(f"Datos descargados de {url}: {response.content}")
       return response.content
   else:
       return None


def procesar_datos(data, fuente_datos):
   #print(f"Datos recibidos: {data}")


   # Determinar el prefijo basado en la fuente de datos
   if 'listado1.xml' in fuente_datos:
       prefijo = 'LIS1-'
   elif 'listado2.xml' in fuente_datos:
       prefijo = 'LIS2-'
   else:
       prefijo = ''


   root = ElementTree.fromstring(data)
   camaras = []
   if root.tag == 'camaras':
       for cam in root.findall('camara'):
           id_camara = prefijo + cam.find('id').text
           url = cam.find('src').text
           info = cam.find('lugar').text
           coordenadas = cam.find('coordenadas').text
           latitud, longitud = map(float, coordenadas.split(','))
           # los campos deben coincidir con los nombres que definimos en el modelo Camera
           camaras.append({
               'identifier': id_camara,
               'image_url': url,
               'location_name': info,
               'latitude': latitud,
               'longitude': longitud
           })
   elif root.tag == 'list':
       for cam in root.findall('cam'):
           id_camara = prefijo + cam.attrib['id']
           url = cam.find('url').text
           info = cam.find('info').text
           place = cam.find('place')
           latitud = float(place.find('latitude').text)
           longitud = float(place.find('longitude').text)
           camaras.append({
               'identifier': id_camara,
               'image_url': url,
               'location_name': info,
               'latitude': latitud,
               'longitude': longitud
           })
   #print(f"Cámaras procesadas: {camaras}")
   return camaras




"""Creamos un procesador de contexto para que actualice nuestras variables de pie de pg"""
def contador(request):
   total_camaras = Camera.objects.count()
   total_comentarios = Comentario.objects.count()
   return {
       'total_camaras': total_camaras,
       'total_comentarios': total_comentarios
   }



