import random
import uuid
import requests
from PIL import Image
from io import BytesIO
from datetime import datetime
from PIL import UnidentifiedImageError


from django.http import Http404, JsonResponse
from django.shortcuts import render, redirect, get_object_or_404
from django.utils import timezone
from django.views.decorators.http import require_POST
from django.contrib.auth import logout


from .models import Camera, Comentario, Configuracion, SessionData
from .forms import ComentarioForm, ConfiguracionForm
from django.core.paginator import Paginator, EmptyPage
from .utils import descargar_fuente, procesar_datos
from django.db.models import Count
from django.urls import reverse




# Create your views here.


"""Defino la vista para la página principal"""
# Defino la vista para la página principal
def pagina_principal(request):
   # Obtener el identificador de la sesión de autorización
   id_autorizacion = request.GET.get('id')
   if id_autorizacion:
       try:
           # buscar los datos de la sesión con el identificador dado
           session_data = SessionData.objects.get(id=id_autorizacion)
           # copiar los datos de la sesión
           #print("datos de la sesion en formato dict: ", session_data.data)
           for key, value in session_data.data.items():
               request.session[key] = value
       except SessionData.DoesNotExist:
           pass


   # Obtener todos los comentarios ordenados por fecha de creación descendente
   comentarios_list = Comentario.objects.order_by('-fecha_creacion')
   paginator = Paginator(comentarios_list, 3) # 3 comentarios por página


   page = request.GET.get('page')  # Obtener el número de página actual


   try:
       comentarios = paginator.get_page(page)  # Obtener los comentarios en la página actual
   except EmptyPage:
       # Para manejar el error, redirigimos al usuario a la última página:
       comentarios = paginator.page(paginator.num_pages)


   # Vamos a jugar con el paginator
   #print("page:", page)
   #print("comentarios en la pagina ",page , len(comentarios))
   #for comentario in comentarios:
   #    print(comentario.texto)  # Imprime el texto de cada comentario


   # Lógica para obtener la cantidad total de cámaras y comentarios
   # contamos el número de filas o registros de cada tabla en la bd
   total_camaras = Camera.objects.count()
   total_comentarios = Comentario.objects.count()


   # Aquí obtenemos el nombre del comentador, puede provenir de alguna autenticación o configuración previa
   nombre_comentador = request.session.get('nombre_comentador')
   if not nombre_comentador:
       nombre_comentador = "Anónimo"
   #print("campos de la solicitud: ", nombre_comentador)


   # Obtener la configuración de la sesión
   configuracion_id = request.session.get('configuracion_id')
   configuracion = None
   if configuracion_id is not None:
       configuracion = Configuracion.objects.get(id=configuracion_id)


   # Pasar los datos a la plantilla
   context = {
       'comentarios': comentarios,
       'total_camaras': total_camaras,
       'total_comentarios': total_comentarios,
       'nombre_comentador': nombre_comentador,
       'configuracion': configuracion
   }


   return render(request, 'Camaras/pagina_principal.html', context)




# Función auxiliar para obtener la fecha de último comentario de una cámara (NO USADA)
def obtener_ultima_fecha_comentario(camara):
   if camara.last_comment_date:
       return camara.last_comment_date
   else:
       return datetime.min




"""Definimos la página para las cámaras"""
def pagina_camaras(request):
   # - Lista de fuentes de datos
   fuentes_datos = [
       'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado1.xml',
       'https://gitlab.eif.urjc.es/cursosweb/2023-2024/final-teveo/-/raw/main/listado2.xml',
   ]


   # - Obtener todas las cámaras y apuntar cada una con el número de comentarios que tiene y ordenar
   # de más a menos comentarios
   camaras = Camera.objects.annotate(num_comentarios=Count('comentarios')).order_by('-num_comentarios')


   # - Crear un objeto Paginator, pasándole la lista de cámaras y el num de cámaras por página
   paginator = Paginator(camaras, 4)   # Mostrar 4 cámaras por página


   # - Obtener la página que el usuario ha solicitado
   numero_pagina = request.GET.get('page', 1)


   try:
       # - Intentar obtener las cámaras de la página solicitada
       pagina_camaras = paginator.page(numero_pagina)
   except EmptyPage:
       # Si el número de página es mayor que el número total de páginas, mostrar la última página
       pagina_camaras = paginator.page(paginator.num_pages)


   # - Seleccionar una cámara aleatoria de la base de datos
   camara_aleatoria = None
   count = camaras.count()     # camaras es un QuerySet
   if count > 0:
       random_index = random.randint(0, count - 1)
       camara_aleatoria = camaras[random_index]


   # - Pasar las cámaras a la plantilla
   context = {
       'camaras': pagina_camaras,
       'fuentes_datos': fuentes_datos,
       'camara_aleatoria': camara_aleatoria,
   }


   return render(request, 'Camaras/pagina_camaras.html', context)




"""Definimos una función para descargar la fuente de datos"""
@require_POST
def descargar_datos(request):
   # Obtener la url de la fuente de datos de la solicitud POST
   fuente_datos = request.POST.get('fuente_datos')


   # Descargar los datos de la cámara
   data = descargar_fuente(fuente_datos)


   # Procesar los datos descargados
   camaras = procesar_datos(data, fuente_datos)


   # Para cada cámara en los datos
   for camara_data in camaras:
       # Comprobar si una cámara con el mismo identificador ya existe
       if not Camera.objects.filter(identifier=camara_data['identifier']).exists():
           # Si no existe, crear una nueva cámara en la base de datos
           camara = Camera.objects.create(
               identifier=camara_data['identifier'],
               image_url=camara_data['image_url'],
               location_name=camara_data['location_name'],
               latitude=camara_data['latitude'],
               longitude=camara_data['longitude']
           )


           #print(f"Cámara guardada: {camara}")


   # Redirigir al usuario de vuelta a la página de cámaras
   return redirect('pagina_camaras')




"""Definimos la página para cada cámara"""
def pagina_camara(request, identifier):
   # Obtener la cámara correspondiente al identificador proporcionado
   camara = get_object_or_404(Camera, identifier=identifier)


   # Obtener todos los comentarios asociados a esta cámara
   comentarios = Comentario.obtener_comentarios_ordenados(camara)


   # Pasar los datos a la plantilla
   context = {
       'camara': camara,
       'comentarios': comentarios
   }


   return render(request, 'Camaras/pagina_camara.html', context)




"""Definimos la página para crear un comentario"""
def poner_comentario(request, camara_id=None):
   # Obtener el identificador de la cámara
   identifier = request.GET.get('identifier')
   print("request GET:", request.GET)
   if not identifier:
       raise Http404("No se ha proporcionado un identificador de cámara")


   # Obtener la cámara con su identificador
   if identifier:
       camara = get_object_or_404(Camera, identifier=identifier)
   else:
       camara =get_object_or_404(Camera, id=camara_id)


   # Procesamos la solicitud HTTP POST donde viene el comentario en el cuerpo de la solicitud
   if request.method == 'POST':
       form = ComentarioForm(request.POST)
       if form.is_valid():
           # Descargar la imagen de la cámara
           response = requests.get(camara.image_url)
           print("response: ", response)
           try:
               imagen = Image.open(BytesIO(response.content))
               print("respuesta de código de estado: ", response.status_code)


               # Convertir la imagen en un objeto de archivo en memoria
               archivo_imagen = BytesIO()
               imagen.save(archivo_imagen, 'JPEG')
               archivo_imagen.seek(0)


               # Crear el comentario y guardar la imagen
               comentario = form.save(commit=False)
               comentario.camara = camara
               comentario.fecha_creacion = timezone.now()
               comentario.imagen_camara.save(f'{identifier}.jpg', archivo_imagen)
               comentario.save()


               # Verificar si la solicitud es una solicitud AJAX
               if request.headers.get('x-requested-with') == 'XMLHttpRequest':
                   return JsonResponse({'status': 'ok'})
               else:
                   return redirect('pagina_camara', identifier=identifier)


           except UnidentifiedImageError:
               print("La imagen descargada no es válida")


           return redirect('pagina_camara', identifier=identifier)
   else:
       form = ComentarioForm()
       print(f"Formulario no válido: {form.errors}")


   context = {
       'form': form,
       'camara': camara,
       'fecha_hora_actual': timezone.now(),
   }


   return render(request, 'Camaras/poner_comentarios.html', context)




"""Vista extra para votar por una cámara"""
def votar_camara(request, identifier):
   # Obtener la cámara con el identificador especificado
   camara = get_object_or_404(Camera, identifier=identifier)
   camara.votos += 1
   camara.save()
   return redirect('pagina_camara', identifier=identifier)




"""Definimos la página dinámica de cada cámara"""
def pagina_dinamica_camara(request, camara_id):
   camara = Camera.objects.get(id=camara_id)
   comentarios = Comentario.objects.filter(camara=camara).order_by('-fecha_creacion')


   context = {
       'camara': camara,
       'comentarios': comentarios
   }


   return render(request, 'Camaras/pagina_dinamica_camara.html', context)




"""Definimos la función para cargar el formulario de comentarios"""
def formulario_comentario(request, camara_id):
   camara = get_object_or_404(Camera, id=camara_id)


   if request.method == 'POST':
       print(f"Datos de la solicitud POST: {request.POST}")  # Agregar esta línea
       form = ComentarioForm(request.POST)
       if form.is_valid():
           comentario = form.save(commit=False)
           comentario.camara = camara
           comentario.fecha_creacion = timezone.now()
           comentario.save()
           print(f"Comentario guardado: {comentario.texto}")  # Agregar esta línea
           return redirect('pagina_dinamica_camara', camara_id=camara_id)
       else:
           print(f"Formulario no válido: {form.errors}")  # Agregar esta línea
           return JsonResponse({'status': 'error', 'errors': form.errors})
   else:
       form = ComentarioForm()
       return render(request, 'Camaras/poner_comentarios.html', {'form': form, 'camara': camara})




"""Definimos la función que devuelve comentarios en formato JSON"""
def obtener_comentarios(request, camara_id):
   camara = get_object_or_404(Camera, id=camara_id)
   comentarios = Comentario.objects.filter(camara=camara).order_by('-fecha_creacion')
   comentarios_data = list(comentarios.values('texto', 'fecha_creacion'))  # convertir el QuerySet a una lista de diccionarios
   return JsonResponse(comentarios_data, safe=False)




"""Definimos la página de configuración"""
def pagina_configuracion(request):
   if request.method == 'POST':
       form = ConfiguracionForm(request.POST)
       if form.is_valid():
           configuracion = form.save()
           # Guardar la configuración en la sesión
           request.session['configuracion_id'] = configuracion.id
           # Guardar el nombre del comentador en la sesión
           request.session['nombre_comentador'] = configuracion.nombre_comentador
           # Guardar la configuración de la fuente en la sesión
           request.session['tipo_fuente'] = configuracion.tipo_fuente
           request.session['tamanio_fuente'] = configuracion.tamanio_fuente


           # Convertir el tamaño de la fuente a píxeles
           tamanio_fuente = configuracion.tamanio_fuente
           if tamanio_fuente == 'pequeño':
               tamanio_fuente_px = 12
           elif tamanio_fuente == 'mediano':
               tamanio_fuente_px = 16
           else:  # 'grande'
               tamanio_fuente_px = 20
           request.session['tamanio_fuente'] = tamanio_fuente_px
       else:
           # Para depurar los errores del formulario
           print(f"Formulario no válido: {form.errors}")
   else:
       form = ConfiguracionForm()


   return render(request, 'Camaras/pagina_configuracion.html', {'form': form})




"""Definimos la página de ayuda"""
def pagina_ayuda(request):
   return render(request, 'Camaras/pagina_ayuda.html')




"""Definimos una vista de cierre de sesión"""
def cerrar_sesion(request):
   logout(request)
   return redirect('/')




"""Definimos una vista para devolver cámaras en formato JSON"""
def camara_json(request, identifier):
   camara = Camera.objects.get(identifier=identifier)
   data = {
       "nombre": camara.identifier,
       "descripcion": camara.location_name,
       "numero_comentarios": camara.comentarios.count(),
       "latitude": camara.latitude,
       "longitude": camara.longitude,
       "image_url": camara.image_url,
       "votos": camara.votos,
       # Si queremos añadir más campos los añado aquí
   }
   return JsonResponse(data)




"""Definimos una vista para generar un enlace de autenticación"""
def generar_enlace_autenticacion(request):
   # Generar identificador único
   id = uuid.uuid4()
   # Guardar el identificador de la sesión
   request.session['id_autorizacion'] = str(id)
   # Guardar los datos de la sesión en la base de datos
   session_data = SessionData(id=str(id), data={
       'configuracion_id': request.session.get('configuracion_id'),
       'nombre_comentador': request.session.get('nombre_comentador'),
       'tipo_fuente': request.session.get('tipo_fuente'),
       'tamanio_fuente': request.session.get('tamanio_fuente'),
   })
   session_data.save()
   # Redirigir al usuario a la URL con el identificador en la query string
   return redirect(reverse('pagina_principal') + f'?id={id}')







