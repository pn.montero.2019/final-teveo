from django.contrib import admin
from .models import Camera, Comentario

# Register your models here.
admin.site.register(Camera)
admin.site.register(Comentario)