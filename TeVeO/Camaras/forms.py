from django import forms
from .models import Comentario, Configuracion

class ComentarioForm(forms.ModelForm):
    class Meta:
        model = Comentario
        fields = ['texto']

class ConfiguracionForm(forms.ModelForm):
    class Meta:
        model = Configuracion
        fields = ['nombre_comentador', 'tamanio_fuente', 'tipo_fuente']