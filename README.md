# Final-TeVeO


# ENTREGA CONVOCATORIA JUNIO


# ENTREGA DE PRÁCTICA


## Datos


* Nombre: Paola Nicole Montero Tamayo
* Titulación: Ingeniería en Tecnologías de la Telecomunicación
* Cuenta en laboratorios: paolamt
* Cuenta URJC: pn.montero.2019@alumnos.urjc.es
* Video básico (url): https://www.youtube.com/watch?v=coAf8o0gQ00&t=3s
* Video parte opcional (url): https://www.youtube.com/watch?v=amwkw_Ye3Ew&t=5s
* Despliegue (url): https://paolamontero.pythonanywhere.com
* Contraseñas:
* Cuenta Admin Site: teveo/teveo


## Resumen parte obligatoria


Este proyecto es una aplicación web Django/Python3 que incluye varias aplicaciones Django para implementar
la funcionalidad requerida. Utiliza SQLite3 para el almacenamiento de datos persistente, con tablas definidas
en modelos de Django.


La aplicación proporciona varias páginas, incluyendo una página principal que muestra un listado de comentarios,
una página de cámaras que muestra un listado de fuentes de datos y cámaras junto a dos enlaces, uno a la página
de la cámara en concreto y otra a la página dinámica de la cámara, una página para cada cámara que muestra información
detallada y comentarios sobre la cámara, una página para publicar comentarios, una página dinámica para cada cámara que
actualiza la imagen y los comentarios periódicamente, una página de configuración para personalizar la apariencia del
sitio y una página de ayuda.


El sitio también proporciona datos de cámaras en formato JSON y un mecanismo de autenticación que permite a otros
navegadores acceder al sitio con la misma configuración a través de un enlace de autenticación que contiene datos
de configuración.


Se han realizado pruebas extremo a extremo para cada tipo de recurso que se define en urls.py del proyecto.


## Lista partes opcionales


* Nombre parte: Inclusión de un favicon con el logo de una cámara con la frase "TEVEO".
* Nombre parte: La opción para terminar la sesión.
* Nombre parte: Permitir votar las cámaras la cantidad de veces que se quiera, incluso si el usuario ya votó en la misma imagen.
* Nombre parte: Paginado en la página principal mostrando 4 comentarios por página y en la página de cámaras para listar
* cada 4 cámaras.
*


